package com.ricc.ofertas.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String SP_TOPICS = "sharedPreferencesTopics";

    Spinner spTopics;

    TextView tvTopics;

    Button btnSuscribir, btnDesuscribir;

    private Set<String> mTopicsSet;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spTopics = findViewById(R.id.spTopics);
        tvTopics = findViewById(R.id.tvTopics);
        btnSuscribir = findViewById(R.id.btnSuscribir);
        btnDesuscribir = findViewById(R.id.btnDesuscribir);

        btnSuscribir.setOnClickListener(this);
        btnDesuscribir.setOnClickListener(this);

        configSharedPreferences();

        if (FirebaseInstanceId.getInstance().getToken() != null) {
            Log.i("Token MainActivity", FirebaseInstanceId.getInstance().getToken());
        }
    }

    private void configSharedPreferences() {
        mSharedPreferences = getPreferences(Context.MODE_PRIVATE);

        mTopicsSet = mSharedPreferences.getStringSet(SP_TOPICS, new HashSet<String>());

        showTopics();
    }

    private void showTopics() {
        tvTopics.setText(mTopicsSet.toString());
    }

    @Override
    public void onClick(View view) {
        String topic = getResources().getStringArray(R.array.topicsValues)[spTopics.getSelectedItemPosition()];

        switch (view.getId()) {
            case R.id.btnSuscribir:
                if (!mTopicsSet.contains(topic)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(topic);
                    mTopicsSet.add(topic);
                    saveSharedPreferences();
                }
                break;
            case R.id.btnDesuscribir:
                if (mTopicsSet.contains(topic)) {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
                    mTopicsSet.remove(topic);
                    saveSharedPreferences();
                }
                break;
        }
    }


    private void saveSharedPreferences() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.putStringSet(SP_TOPICS, mTopicsSet);
        editor.apply();

        showTopics();
    }


}