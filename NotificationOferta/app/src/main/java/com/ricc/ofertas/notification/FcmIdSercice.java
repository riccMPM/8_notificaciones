package com.ricc.ofertas.notification;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FcmIdSercice extends FirebaseInstanceIdService {
    public FcmIdSercice() {
    }

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshToken);
    }

    private void sendRegistrationToServer(String refreshToken) {
        //Revisamos en el logcat y ese id lo podemos usar en firebase console para enviar a un dispositivo unico
        Log.d("Token", refreshToken);
    }
}
